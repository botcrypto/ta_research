# Usage

## Pattern recognition

```python pattern_detection <pattern>```

`pattern` is a pattern name from the ta-lib package. You can find available patterns [here](https://github.com/mrjbq7/ta-lib#pattern-recognition).

### Examples

```python pattern_detection CDLDOJI```

```python pattern_detection CDLHAMMER```

### Results

It will save a .csv file following the example below.

||date|close|volumefrom|volumeto|nextclose|variation|
|---|---|---|---|---|---|---|
|0|2019-12-01|7420.53|30676.77|226245528.68|7320.94|-1.342087425022204|
|1|2019-12-02|7320.94|25163.31|184009273.35|7313.64|-0.09971397115669946|
|2|2019-12-25|7202.72|15012.21|108513256.34|7207.23|0.06261523424483123|

1 line = 1 pattern.

# Dependancies

python3

[ta-lib](https://github.com/mrjbq7/ta-lib#dependencies)