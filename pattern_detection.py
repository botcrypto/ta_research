import requests
import pandas as pd
import talib
import sys

patterns = ["CDLHAMMER", "CDLDOJI"]

# link for Bitcoin Data
link = "https://min-api.cryptocompare.com/data/histoday?fsym=BTC&tsym=USD&limit=1825&aggregate=1"

# API request historical
historical_get = requests.get(link)

# access the content of historical api request
historical_json = historical_get.json()

# extract json data as dictionary
historical_dict = historical_json['Data']

# extract Final historical df
df = pd.DataFrame(historical_dict,
                             columns=['close', 'high', 'low', 'open', 'time', 'volumefrom', 'volumeto'],
                             dtype='float64')

# time column is converted to "YYYY-mm-dd hh:mm:ss" ("%Y-%m-%d %H:%M:%S")
posix_time = pd.to_datetime(df['time'], unit='s')

# append posix_time
df.insert(0, "date", posix_time)

# drop unix time stamp
df.drop("time", axis = 1, inplace = True)

op = df['open'].astype(float)
hi = df['high'].astype(float)
lo = df['low'].astype(float)
cl = df['close'].astype(float)

for pattern in patterns:
    df["pattern"] = getattr(talib, pattern)(op, hi, lo, cl)

    new_df = pd.DataFrame(columns=['date', 'close', 'volumefrom', 'volumeto', 'nextclose'],
                                dtype='float64')

    for row in df.itertuples(index=True):
        if(row.pattern == 100):

            nextclose = df.iloc[row.Index + 1,:].close
            if(row.Index + 7 < len(df)):
                nextweekclose = df.iloc[row.Index + 7,:].close
            else:
                nextweekclose = 0

            variation = (nextclose - row.close) / row.close * 100
            nextweekvariation = (nextweekclose - row.close) / row.close * 100
            new_df = new_df.append({ 'date': row.date, 'close': row.close, 'volumefrom': row.volumefrom, 'volumeto': row.volumeto, 'nextclose': nextclose, 'nextweekclose': nextweekclose, 'variation': variation, 'nextweekvariation': nextweekvariation}, ignore_index = True)

    new_df.to_csv(pattern + '.csv')
